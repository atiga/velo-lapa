class Reservation < ApplicationRecord
  belongs_to :bicycle
  belongs_to :user

  def full_type
    case reserv_type
      when "S" 
        "In Service"
      when "R" 
        "Reservation"
      else 
        "Unavailable"
    end
  end

  validates :bicycle, :date_from, :date_to, :user, :description, :reserv_type, presence:true 
  validate :reserv_time

  def reserv_time  
    rt = Reservation.where(["date_from < ? and date_to > ? and bicycle_id = ? and id <> ?", date_to, date_from, bicycle_id, id]).first
    
    if rt.present?
      errors.add(:base, "Unavailable for reservation in the selected period!")
    end
    
  end

end
