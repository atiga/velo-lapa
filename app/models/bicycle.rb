class Bicycle < ApplicationRecord

    def available_from
        av_f= Reservation.where(["date_from <= ? and bicycle_id = ?", Time.current, id]).order("date_to ASC").last
        if av_f.present? and av_f.date_to>Time.current
            "from " + av_f.date_to.strftime("%H:%M %a %d-%b-%Y")
        else "from now" 
        end
    end

    def available_till
        af_t= Reservation.where(["date_from > ? and bicycle_id = ?", Time.current, id]).order("date_from ASC").first
        if af_t.present?
            "till " + af_t.date_from.strftime("%H:%M %a %d-%b-%Y")
        else "with no end time" 
        end
    end

    validates :frame_number, :name, :description, presence:true
    validates :frame_number, uniqueness: true
    validates :name, uniqueness: true
end
