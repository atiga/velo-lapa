json.extract! reservation, :id, :bicycle_id, :date_from, :date_to, :user_id, :description, :reserv_type, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
