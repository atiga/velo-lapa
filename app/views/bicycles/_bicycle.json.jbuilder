json.extract! bicycle, :id, :frame_number, :name, :description, :created_at, :updated_at
json.url bicycle_url(bicycle, format: :json)
