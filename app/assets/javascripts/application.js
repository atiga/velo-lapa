//= require rails-ujs
//= require jquery
//= require bootstrap-sprockets

$(document).ready(function(){
setInterval(function(){
    $.ajax({
        url: "/reservations/time_reservations"
    }).done(function(reservations) {
        for(let reservation of reservations) {
            let $reservationElement = $('[data-reservation-id="' + reservation.id + '"]');
            $reservationElement.find('[data-reservation-attribute="bicycle"]').text(reservation.bicycle.name);
            $reservationElement.find('[data-reservation-attribute="date_from"]').text(reservation.date_from);
            $reservationElement.find('[data-reservation-attribute="date_to"]').text(reservation.date_to);
            $reservationElement.find('[data-reservation-attribute="user"]').text(reservation.user.user_name);
            $reservationElement.find('[data-reservation-attribute="description"]').text(reservation.description);
            $reservationElement.find('[data-reservation-attribute="full_type"]').text(reservation.full_type);
        }
    });  
    
    $.ajax({
        url: "/bicycles/time_bicycles"
    }).done(function(bicycles) {
        for(let bicycle of bicycles) {
            let $bicycleElement = $('[data-bicycle-id="' + bicycle.id + '"]');
            $reservationElement.find('[data-reservation-attribute="bicycle"]').text(reservation.bicycle.name);
            $reservationElement.find('[data-reservation-attribute="date_from"]').text(reservation.date_from);
            $reservationElement.find('[data-reservation-attribute="date_to"]').text(reservation.date_to);
            $reservationElement.find('[data-reservation-attribute="user"]').text(reservation.user.user_name);
            $reservationElement.find('[data-reservation-attribute="description"]').text(reservation.description);
            $reservationElement.find('[data-reservation-attribute="full_type"]').text(reservation.full_type);
        }
    });   
}, 5000);
}); 