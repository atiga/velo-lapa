class CreateBicycles < ActiveRecord::Migration[5.2]
  def change
    create_table :bicycles do |t|
      t.string :frame_number
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
