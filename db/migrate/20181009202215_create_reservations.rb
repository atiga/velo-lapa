class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.references :bicycle, foreign_key: true
      t.datetime :date_from
      t.datetime :date_to
      t.references :user, foreign_key: true
      t.string :description
      t.string :reserv_type

      t.timestamps
    end
  end
end
