# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
users = User.create([
    {name:"Mickey", surname: "Mouse"},
    {name:"Donald", surname: "Duck"},
    {name:"Minney", surname: "Mouse"},
    {name:"Daisy", surname: "Duck"},
    {name:"Scrooge", surname: "McDuck"},
    {name:"Ludwig", surname: "Von Drake"},
    {name:"Webby", surname: "Vanderquack"},
    {name:"José", surname: "Carioca"},
    {name:"Bentina", surname: "Beakley"},
    {name:"Pistol", surname: "Pete"},
    {name:"Webby", surname: "Vanderquack"}
])
bicycles = Bicycle.create([
    {frame_number:"BE1234", name:"Bee", description:"Black and Yellow"},
    {frame_number:"FL2345", name:"Fly", description:"Black and Silver"},
    {frame_number:"DR3456", name:"Dragonfly", description:"Green and Purple"},
    {frame_number:"SP4567", name:"Spider", description:"Black"},
    {frame_number:"AN5678", name:"Ant", description:"Red"},
])