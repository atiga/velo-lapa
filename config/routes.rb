Rails.application.routes.draw do
  resources :bicycles
  resources :users
  resources :reservations do
    collection do
      get 'time_reservations'
    end
  end
  root 'bicycles#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
